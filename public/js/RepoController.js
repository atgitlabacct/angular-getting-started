(function() {
  var app = angular.module("githubViewer");

  var repoController = function($scope, github, $routeParams) {

    $scope.username = $routeParams.username;
    $scope.reponame = $routeParams.reponame;

    var onRepoComplete = function(data) {
      console.log(data);
      $scope.repoDetails = data;
    };

    var onError = function(reason) {
      $scope.error = reason;
    };

    github.getRepoDetails($scope.username, $scope.reponame)
      .then(onRepoComplete, onError);
  }

  app.controller("RepoController", ["$scope", "github", "$routeParams",
                 repoController]);
}());
