(function() {

  var github = function($http) {

    var githubApi = "https://api.github.com/"

    var getUser = function(username) {
      return $http.get(githubApi + "users/" + username)
        .then(function(response) {
          return response.data;
        });
    };

    var getRepos = function(user) {
      return $http.get(user.repos_url)
        .then(function(response) {
             return response.data;
        });
    };

    var getRepoDetails = function(username, reponame) {
      var repo;
      
      return $http.get(githubApi + "repos/" + username + "/" + reponame)
        .then(function(response) {
          repo = response.data;
          console.log(repo);
          return $http.get(repo.contributors_url);
        })
        .then(function(response) {
          repo.contributors = response.data;
          return repo;
        });
    };

    var getContributors = function(url) {
      return $http.get(url)
        .then(function(response) {
          return response.data;
        });
    }

    return {
      getUser: getUser,
      getRepos: getRepos,
      getRepoDetails: getRepoDetails,
      getContributors: getContributors
    };

  };

  var module = angular.module("githubViewer");
  module.factory("github", github);

}());
